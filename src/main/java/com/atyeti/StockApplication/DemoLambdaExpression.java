package com.atyeti.StockApplication;

import java.util.ArrayList;
import java.util.Collections;

import java.util.List;

public class DemoLambdaExpression {
public static void main(String args[]) {
List<Student> ar = new ArrayList<Student>();
ar.add(new Student("bbbb", "london", 111));
ar.add(new Student( "aaaa","nyc", 131));
ar.add(new Student( "cccc","jaipur", 121));
System.out.println("sorting on the basis of name");
/*Collections.sort(ar,new Comparator<Student>() {//anonymous method

 @Override
public int compare(Student o1, Student o2) {
return o1.name.compareTo(o2.name);
}
});

for(Student s:ar) {
System.out.println(s.rollno+" "+s.name+" "+s.address);
}
*/
Collections.sort(ar,(p1,p2)->{
return p1.name.compareTo(p2.name);
});

for(Student s:ar) {
System.out.println(s.rollno+" "+s.name+" "+s.address);
}
}
}